﻿using QuandParie.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuandParie.Core.Persistance
{
    public interface IWagerRepository
    {
        Task SaveAsync(Wager wager);
        Task<IReadOnlyList<Wager>> GetWagerHavingOdds(Guid id);
        Task<IReadOnlyList<Wager>> GetWagerOfUser(string email);
    }
}